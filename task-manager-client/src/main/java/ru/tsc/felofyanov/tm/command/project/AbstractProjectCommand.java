package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.command.AbstractCommand;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.DateUtil;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        @NotNull final Status status = project.getStatus();
        System.out.println("STATUS: " + status.getDisplayName());
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }
}
