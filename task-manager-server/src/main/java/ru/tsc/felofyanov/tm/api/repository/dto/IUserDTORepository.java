package ru.tsc.felofyanov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IRepositoryDTO<UserDTO> {

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    UserDTO removeByLogin(@Nullable String login);
}
