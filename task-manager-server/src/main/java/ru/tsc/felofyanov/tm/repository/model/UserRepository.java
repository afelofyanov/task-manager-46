package ru.tsc.felofyanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.IUserRepository;
import ru.tsc.felofyanov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public class  UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager, User.class);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        final TypedQuery<User> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE login = :login", clazz)
                        .setHint("org.hibenate.cacheable", true)
                        .setParameter("login", login);
        @NotNull final List<User> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable String email) {
        final TypedQuery<User> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE email = :email", clazz)
                        .setHint("org.hibenate.cacheable", true)
                        .setParameter("email", email);
        @NotNull final List<User> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public User removeByLogin(@Nullable String login) {
        @Nullable final Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }
}
