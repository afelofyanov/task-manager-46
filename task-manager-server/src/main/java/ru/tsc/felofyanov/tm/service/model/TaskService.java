package ru.tsc.felofyanov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.ITaskRepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.model.ITaskService;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskRepository repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
