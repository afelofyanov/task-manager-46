package ru.tsc.felofyanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.ITaskRepository;
import ru.tsc.felofyanov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager, Task.class);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId", clazz)
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        entityManager.createQuery("DELETE FROM " + getModelName() + " WHERE user_id = :userId AND project_Id = :projectId")
                .setParameter("projectId", projectId)
                .setParameter("userId", userId)
                .executeUpdate();
    }
}
