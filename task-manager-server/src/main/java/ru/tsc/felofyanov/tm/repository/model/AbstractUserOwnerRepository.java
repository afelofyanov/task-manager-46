package ru.tsc.felofyanov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.model.IUserOwnerRepository;
import ru.tsc.felofyanov.tm.model.AbstractWbs;
import ru.tsc.felofyanov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnerRepository<M extends AbstractWbs>
        extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @NotNull
    protected List<M> result = new ArrayList<>();

    public AbstractUserOwnerRepository(@NotNull EntityManager entityManager, @NotNull Class<M> clazz) {
        super(entityManager, clazz);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUser(user);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUser(user);
        model.setDescription(description);
        return add(model);
    }


    @NotNull
    @Override
    public List<M> findAllByUserId(@Nullable String userId) {
        return entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId", clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        entityManager.createQuery("DELETE FROM " + getModelName() + " WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsByIdUserId(@NotNull String userId, @NotNull String id) {
        return findOneByIdUserId(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneByIdUserId(@Nullable String userId, @Nullable String id) {
        final TypedQuery<M> query =
                entityManager.createQuery("FROM " + getModelName() + " WHERE user_id = :userId AND id = :id", clazz)
                        .setParameter("userId", userId)
                        .setParameter("id", id);
        @NotNull final List<M> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Nullable
    @Override
    public M findOneByIndexByUserId(@Nullable String userId, @Nullable Integer index) {
        final TypedQuery<M> query = entityManager.createQuery("FROM " + getModelName() + "WHERE user_id = :userId", clazz)
                .setParameter("userId", userId)
                .setFirstResult(index);
        @NotNull final List<M> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public M remove(@Nullable String userId, @Nullable M model) {
        if (userId == null || model == null) return null;
        return removeByIdByUserId(userId, model.getId());
    }

    @Override
    public M removeByIdByUserId(@Nullable String userId, @Nullable String id) {
        if (userId == null || id == null) return null;
        final Optional<M> model = Optional.ofNullable(findOneByIdUserId(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public M removeByIndexByUserId(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || index == null) return null;
        Optional<M> model = Optional.ofNullable(findOneByIndexByUserId(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public long countByUserId(@Nullable String userId) {
        return entityManager.createQuery("SELECT COUNT(1) FROM " + getModelName() + " WHERE user_id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }
}
