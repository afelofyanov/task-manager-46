package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProjectUnbindTaskByIdResponse extends AbstractProjectResponse {
}
