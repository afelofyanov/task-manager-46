package ru.tsc.felofyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataSaveBinaryRequest extends AbstractUserRequest {

    public DataSaveBinaryRequest(@Nullable String token) {
        super(token);
    }
}
