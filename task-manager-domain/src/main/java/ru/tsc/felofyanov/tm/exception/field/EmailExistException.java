package ru.tsc.felofyanov.tm.exception.field;

public final class EmailExistException extends AbstractFieldException {
    public EmailExistException() {
        super("Error! Email already exists...");
    }
}
