package ru.tsc.felofyanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractWbsDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

    public TaskDTO(@Nullable final String userId, @NotNull final String name) {
        super(userId, name);
    }

    public TaskDTO(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        super(userId, name, description);
    }

    public TaskDTO(@NotNull final String name, @NotNull final Status status, @NotNull final Date created) {
        super(name, status, created);
    }
}
